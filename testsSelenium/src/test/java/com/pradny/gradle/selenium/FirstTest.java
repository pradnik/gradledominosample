package com.pradny.gradle.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FirstTest {
	private WebDriver driver;
	private String browser;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@BeforeMethod
	public void setUp(ITestContext context) throws Exception {
		this.browser=browser;
		WebDriver basedriver;

		System.out.println("Firefox driver would be used");
		basedriver = new FirefoxDriver();
	    driver=basedriver;
	    context.setAttribute("driver", driver);
    
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    baseUrl =  System.getProperty("BASE_URL", "http://localhost/");
	}
	
	 @Test
	  public void testCalculation() throws Exception{
		  driver.get(baseUrl);
		  driver.findElement(By.id("view:_id1:inputField")).sendKeys("100");
		  driver.findElement(By.id("view:_id1:calcButton")).click();
		  
		  assertEquals(driver.findElement(By.id("view:_id1:result")).getText(),"200");
	  }
	 
	 @AfterMethod
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }






}
