package com.pradny.test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

@Test
public class SimpleControllerTest {

	private static final Long ANY_NUMBER = 333l;
	SimpleController sc;
	
	@BeforeMethod
	protected void setUp(){
		sc=new SimpleController();
	}
	
	public void multiplyTest(){
		assertEquals(sc.getMultipliedValue(ANY_NUMBER),(Long)(ANY_NUMBER*2));
	}
}
