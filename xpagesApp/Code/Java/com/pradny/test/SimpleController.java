package com.pradny.test;

import java.io.Serializable;

public class SimpleController implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Long getMultipliedValue(Long i){
		return 2*i;
	}
}
